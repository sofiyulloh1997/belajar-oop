<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $object1 = new animal("shaun");

    echo "Name : $object1->name <br>";
    echo "Legs : $object1->legs <br>";
    echo "Cold Blooded : $object1->cold_blooded <br><br>";

    $object2 = new frog ("buduk");
    echo "Name : $object2->name <br>";
    echo "Legs : $object2->legs <br>";
    echo "Cold Blooded : $object2->cold_blooded <br>";
    echo $object2->jump() ."<br><br>";

    $object3 = new ape ("kera sakti");
    echo "Name : $object3->name <br>";
    echo "Legs : $object3->legs <br>";
    echo "Cold Blooded : $object3->cold_blooded <br>";
    $object3->yell();


?>